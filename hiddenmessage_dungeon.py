import collections


def main(*, phrase: str, filename: str):
    phrase = phrase.replace(' ', '')
    text = []
    gods = []
    heroes = []
    with open(filename, 'r') as fileobj:
        # skip header
        for line in fileobj:
            if line.startswith('Союзники'):
                break
        # parse party
        for line in fileobj:
            if line.startswith('Хроника'):
                break
            if line[0] in '0123456789':
                # skip health
                continue
            if line[0] == '(':
                gods.append(line[1:-2])
                continue
            heroes.append(line[:-1])
        # parse logs
        for line in fileobj:
            if line[0] in '0123456789':
                # skip timestamp
                continue
            if line.startswith('шаг'):
                # skip step counter
                continue
            if '☣' in line:
                # skip voices
                continue
            if line == 'Карта\n':
                # skip footer
                break
            # TODO: skip influences?..
            line_text = line[:-1] if line.endswith('\n') else line
            for hero in heroes:
                line_text = line_text.replace(hero, '%random_hero%')
            text.append(line_text)
    goal = text[-1].split('.')[0]
    text[-1] = goal
    letters = collections.Counter(''.join(text))
    phrase_letters = collections.Counter(phrase)
    matches = all(letters[letter] >= count for letter, count in phrase_letters.items())
    if matches:
        print('WIN')
        for letter, count in phrase_letters.items():
            print(f'{letter}({count}): ')
            counter = 0
            for line in text:
                if letter in line:
                    counter += 1
                    print('\t', line)
                    if counter >= count:
                        break
        return
    print('LOOSE')


if __name__ == '__main__':
    import sys

    main(phrase=sys.argv[1], filename=sys.argv[2])
