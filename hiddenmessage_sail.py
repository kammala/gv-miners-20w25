import collections
from typing import List, Dict

import bs4

import requests


def check_log(*, phrase: str, log_id: str) -> Dict[str, List[str]]:
    text = _parse_log(log_id)

    phrase_letters = collections.Counter(phrase)
    result = {}
    for letter, count in phrase_letters.items():
        for line in text:
            if letter in line:
                result.setdefault(letter, []).append(line)
        found_letters = len(result.get(letter, []))
        if found_letters < count:
            print(f'{log_id} :: not enough "{letter}": needs {count}, has {found_letters}')
            return {}
    return result


def _parse_log(log_id: str) -> List[str]:
    log = requests.get(f'https://gv.erinome.net/duels/log/{log_id}')
    log.raise_for_status()

    parsed = bs4.BeautifulSoup(log.text, 'html.parser')


    heroes = [
        participant.text.split('/')[0].split(' ', 1)[-1].strip()
        for participant in parsed.find_all('div', class_='c1')
    ]

    text = [
        hide_names(
            text=tag.text.strip(), names=heroes, replacement='%random_hero%'
        )
        for tag in parsed.select('div.text_content:not(.infl)')
    ]
    return text


def hide_names(*, text: str, names: List[str], replacement: str):
    for name in names:
        text = text.replace(name, replacement)
    return text


def main(phrase: str):
    logs = [
        '2hzc114',
        '3ae6rqm',
        '5gf0psb',
        '7yw286x',
        '9fw2slj',
        '9jnab4h',
        'drg9lqj',
        'ebpwsmm',
        'mwcz8nr',
        'qcbfngg',
        't3zed71',
        'ugugqqh',
        'w4wuah3',
        'w9eeg69',
    ]

    for log_id in logs:
        result = check_log(phrase=phrase, log_id=log_id)
        if not result:
            print(f'{log_id} :: LOOSE')
            continue

        print(f'{log_id} :: WIN')
        for letter in phrase:
            print(letter, ':', result[letter].pop())


if __name__ == '__main__':
    main('ПОМОГИТЕ')
